/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.koolitus.unittestdemo;

import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author heleen
 */
public class SalaryTest {

    private static final BigDecimal CROSS_SALARY_BELOW_EXEMPTION = BigDecimal.valueOf(160);
    private static final BigDecimal CROSS_SALARY_ABOVE_EXEMPTION = BigDecimal.valueOf(1000);
    Salary salaryBelowExemption;
    Salary salaryAboveExemption;

    @Before
    public void setUp() {
        salaryBelowExemption = new Salary(CROSS_SALARY_BELOW_EXEMPTION);
        salaryAboveExemption = new Salary(CROSS_SALARY_ABOVE_EXEMPTION);
    }

    @Test
    public void testSalaryGrossWageIsSet() {
        Assert.assertEquals(salaryBelowExemption.getSalaryGrossWage(), CROSS_SALARY_BELOW_EXEMPTION);
        Assert.assertEquals(salaryAboveExemption.getSalaryGrossWage(), CROSS_SALARY_ABOVE_EXEMPTION);
    }

    @Test
    public void testNetSalaryBelowExemption() {
        Assert.assertEquals(salaryBelowExemption.getSalaryNetWage(), CROSS_SALARY_BELOW_EXEMPTION);
    }

    @Test
    public void testNetSalaryAboveExemption() {
        final BigDecimal netSalaryExpected = CROSS_SALARY_ABOVE_EXEMPTION.subtract(BigDecimal.valueOf(180))
                .multiply(BigDecimal.valueOf(0.8));
        Assert.assertEquals(salaryAboveExemption.getSalaryNetWage(), netSalaryExpected);
    }
}
