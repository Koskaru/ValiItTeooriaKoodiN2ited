/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.serializedemo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author heleen
 */
public class SerializeDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Car c1 = new Car("Mersedes", 2000, BigDecimal.valueOf(100.00));
        Car c2 = new Car("Toyota", 2005, BigDecimal.valueOf(150.00));

        CarRent cr = new CarRent(c1, c2);
        System.out.println("Enne faili kirjutamist: " + cr);
        try (FileOutputStream output = new FileOutputStream("resourses/serializeDemoOut.txt")) {
            ObjectOutputStream objectStream = new ObjectOutputStream(output);
            objectStream.writeObject(cr);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        CarRent crFromFile = null;
        try (FileInputStream input = new FileInputStream("resourses/serializeDemoOut.txt")) {
            ObjectInputStream objectStream = new ObjectInputStream(input);
            crFromFile = (CarRent) objectStream.readObject();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializeDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Failist: " + crFromFile);
    }
}
