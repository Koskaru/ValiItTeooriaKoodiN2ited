/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.serializedemo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author heleen
 */
public class Car implements Serializable {

    private String modelName;
    private int productionYear;
    private transient BigDecimal currentPrice;

    public Car(String modelName, int productionYear, BigDecimal currentPrice) {
        this.modelName = modelName;
        this.productionYear = productionYear;
        this.currentPrice = currentPrice;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    @Override
    public String toString() {
        return "Car{" + "modelName=" + modelName + ", productionYear=" + productionYear + "\n, currentPrice=" + currentPrice + '}';
    }
}
