/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit;

/**
 *
 * @author heleen
 */
public class StringSplit {

    public static void main(String[] args) {
        String textContainingListOfNames = "This is a list of city names: Tallinn, Tartu, Pärnu, Kuressaare";
        String textWithNamesOnly = textContainingListOfNames.split(":")[1];
        String[] arrayOfCityNames = textWithNamesOnly.split(",");
        for (String cityName : arrayOfCityNames) {
            System.out.println(cityName);
        }
    }
}
