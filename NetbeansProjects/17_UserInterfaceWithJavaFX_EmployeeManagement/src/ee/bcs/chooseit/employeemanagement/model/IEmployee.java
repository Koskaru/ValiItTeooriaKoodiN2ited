/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.employeemanagement.model;

import java.math.BigDecimal;

/**
 *
 * @author heleen
 */
public interface IEmployee {
    BigDecimal getSalary();
}
