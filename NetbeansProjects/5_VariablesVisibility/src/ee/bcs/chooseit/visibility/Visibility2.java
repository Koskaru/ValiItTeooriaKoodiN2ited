/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.visibility;

/**
 *
 * @author heleen
 */
public class Visibility2 {
    public void testVariablesInVisibility() {
        Visibility visibility = new Visibility();
        // System.out.println(visibility.privateClassVariable);
        System.out.println(visibility.defaultClassVariable);
        System.out.println(visibility.protectedClassVariable);
        System.out.println(visibility.publicClassVariable);
    }
}
