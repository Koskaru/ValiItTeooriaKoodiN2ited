/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.animals;

/**
 *
 * @author heleen
 */
public enum Leg {
    LEFT("This is left leg"), RIGHT ("This is right leg");
    private final String description;
    
    private Leg(String d) {
        description = d;
    }
    public String getDescription() {
        return description;
    }
}
