/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.collectionsintro;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author heleen
 */
public class SetIntro {
    public static void main(String[] args) {
        simpleSetExample();
        comparatorExample();
    }

    private static void simpleSetExample() {
        Set<String> simpleSet = new TreeSet<>();
        simpleSet.add("text");
        simpleSet.add("alphabet");
        simpleSet.add("horse");
        simpleSet.add("text");
        simpleSet.add("vague");
        simpleSet.forEach(t -> {
            System.out.println(t);
        });
        System.out.println("Set size is " + simpleSet.size()+ "\n");
    }

    private static void comparatorExample() {
        Car Toyota2016 = new Car("Toyota", 2016);
        Car Toyota2010 = new Car("Toyota", 2010);
        Car mazda2010 = new Car("Mazda", 2010);
        Car mazda1999 = new Car("Mazda", 1999);
        Car mercedes2005 = new Car("Mercedes", 2005);
        
        Set<Car> carSet = new TreeSet<>();
        carSet.add(Toyota2016);
        carSet.add(Toyota2010);
        carSet.add(mazda2010);
        carSet.add(mazda1999);
        carSet.add(mercedes2005);
        carSet.add(mazda1999);
        
        carSet.forEach(car -> {
            System.out.println(car);
        });        
    }
}
