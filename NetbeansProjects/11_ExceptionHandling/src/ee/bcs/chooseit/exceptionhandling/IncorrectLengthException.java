package ee.bcs.chooseit.exceptionhandling;

import java.io.IOException;

/**
 *
 * @author heleen
 */
public class IncorrectLengthException extends IOException {

    public IncorrectLengthException() {
        super();
    }

    public IncorrectLengthException(String message) {
        super(message);
    }

}
