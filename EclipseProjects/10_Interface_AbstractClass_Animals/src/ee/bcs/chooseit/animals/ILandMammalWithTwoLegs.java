package ee.bcs.chooseit.animals;

public interface ILandMammalWithTwoLegs {
	void makeStepWithLeg(Leg leg);
}
