package ee.bcs.chooseit.animals;

public class Dolphin  extends Mammal implements ISeaMammal{

    @Override
    public void moveAhead() {
        swimToSurface();
        breathe();
        dive();
        swimAhead();
    }

    @Override
    public void swimAhead() {
        System.out.println("Dolphin swam ahead.");
    }

    @Override
    public void dive() {
        System.out.println("Dolphin dove.");
    }

    private void breathe() {
        System.out.println("Dolphin breathed.");
    }

    private void swimToSurface() {
        System.out.println("Dolphin swam to surface.");
    }
}