package ee.bcs.chooseit.threading;

/**
 *
 * @author heleen
 */
public class ExampleRunnableWithSync implements Runnable {

    static int i = 0;

    @Override
    public void run() {
        synchronized (this) {
            i++;
            System.out.println(Thread.currentThread().getName() + " i = " + i);
        }
    }
}
