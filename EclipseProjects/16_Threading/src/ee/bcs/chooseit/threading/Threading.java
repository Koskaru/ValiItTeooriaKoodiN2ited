package ee.bcs.chooseit.threading;

/**
 *
 * @author heleen
 */
public class Threading {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ExampleThread t = new ExampleThread();
        t.start();

        ExampleRunnable r = new ExampleRunnable();
        Thread t2 = new Thread(r);
        t2.start();

        Thread t3 = new Thread(r);
        t3.start();
        
        /**ExampleRunnableNoSync noSync = new ExampleRunnableNoSync();
        Thread t11 = new Thread(noSync, "First thread");
        t11.start();
        Thread t22 = new Thread(noSync, "Second thread");
        t22.start();
        Thread t33 = new Thread(noSync, "Third thread");
        t33.start();*/
    }
}
