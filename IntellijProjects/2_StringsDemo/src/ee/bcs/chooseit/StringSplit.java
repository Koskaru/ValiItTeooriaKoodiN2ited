package ee.bcs.chooseit;

/**
 * Created by heleen on 26.06.2017.
 */
public class StringSplit {
    public static void main(String[] args) {
        String textContainingListOfNames = "This is a list of city names: Tallinn, Tartu, Pärnu, Kuressaare";
        String textWithNamesOnly = textContainingListOfNames.split(":")[1];
        String[] arrayOfCityNames = textWithNamesOnly.split(",");
        for (String cityName : arrayOfCityNames) {
            System.out.println(cityName);
        }
    }
}
