package ee.bcs.chooseit.animals;

/**
 * Created by heleen on 28.06.2017.
 */
public interface ILandMammalWithFourLegs {
    void makeStepWithDiagonalLegs(Leg frontLeg, Leg backLeg);
}
